
# coding: utf-8

## Pràctiques de Nous Usos de la Informàtica

### Naive Bayes i Classificació

# <b>Lliurament: </b>
# Aquesta pràctica tindrà una durada mínima de 3 setmanes.</b>
# 

# ### Què s’ha de fer?
# 
# Volem classificar el contingut de blogs corresponents a militants de quatre partits polítics segons la seva ideologia. 
# 
# Per fer-ho farem servir les seves entrades RSS o Atom. A partir de les seves entrades crearem un vector de característiques que ens descrigui el contingut del blog. Finalment desenvoluparem un classificador probabilístic del tipus **Naive Bayes** que ens permeti identificar la ideologia d'un nou blog donat la seva descripció en les característiques escollides.
# 
# #### Quina és la idea del sistema de classificació que s’ha de desenvolupar?
# 
# La classificació és un concepte de l'aprenentatge automàtic supervisat. L'objectiu del classificador és, donat un vector de característiques que descriuen els objectes que es volen classificar, indicar a quina categoria o classe pertanyen d'entre un conjunt predeterminat. El procés de classificació consta de dues parts: (a) el procés d'aprenentatge i (b) el procés d'explotació o testeig. 
# 
# El procés d'aprenentatge rep exemples de parelles $(x,y)$ on $x$ són les característiques, usualment nombres reals, i $y$ és la categoria a la que pertanyen. Aquest conjunt se'l coneix com a conjunt d'entrenament i ens servirà per trobar una funció $y=h(x)$ que donada una $x$ m'indiqui quina és la $y$. Per altra banda el procés de testeig aplica la funció $h(x)$ apresa a l'entrenament a una nova descripció per veure quina categoria li correspon.
# 
# #### Classificació i llenguatge natural
# 
# La descripció dels exemples en característiques és el punt més crític de tot sistema d'aprenentatge automàtic. Una de les representacions més simples per tal de descriure un text és la representació bag-of-words. Aquesta representació converteix un text en un vector de $N$ paraules. Alguns dels processos bàscics són la selecció del conjunt d'$N$ paraules i per cada paraula contar quants cops apareix en el text. Una versió alternativa d'aquest procés pot ser simplement indicar si apareix o no en el text.
# 
# #### Exemple.
# 
# Imaginem que tenim 4 texts que pertanyen només a dues categories $y$={'ERC, 'ICV'}, podríem seleccionar les següents paraules per tal de distingir les dues categories $x$={'nuclear', 'verd', 'ecologia', 'independència', 'autonomia', 'referèndum'}
# 
# <table border="1">
# <tr>
# <td></td>
# <td>nuclear</td>
# <td>verd</td>
# <td>ecologia</td>
# <td>independència</td>
# <td>autonomia</td>
# <td>referèndum</td>
# </tr>
# <tr>
# <td>text 1: ('ERC')</td>
# <td>0</td>
# <td>0</td>
# <td>0</td>
# <td>1</td>
# <td>2</td>
# <td>3</td>
# </tr>
# <tr>
# <td>text 2: ('ERC')</td>
# <td>0</td>
# <td>1</td>
# <td>0</td>
# <td>1</td>
# <td>2</td>
# <td>3</td>
# </tr>
# <tr>
# <td>text 3: ('ICV')</td>
# <td>1</td>
# <td>3</td>
# <td>1</td>
# <td>0</td>
# <td>1</td>
# <td>0</td>
# </tr>
# <tr>
# <td>text 4: ('ICV')</td>
# <td>2</td>
# <td>1</td>
# <td>2</td>
# <td>0</td>
# <td>0</td>
# <td>0</td>
# </tr>
# </table>
# 
# 
# amb aquesta representació el text 2, corresponent a la categoria 'ERC', quedaria representat pel vector numèric $(0,1,0,1,2,3)$. Si fem servir la representació alternativa tindríem $(0,1,0,1,1,1)$ que indica la presència de les paraules. Si la descripció és adient s'espera que les categories es puguin distingir amb facilitat.
# 
# #### El classificador Naïve Bayes.
# Un cop tenim una representació necessitem un procés d'aprenentatge que ens permeti passar de la descripció a una categoria. En aquesta pràctica farem servir el classificador Naïve Bayes. Aquest classificador forma part de la família de classificadors probabilístics. La sortida d'un classificador probabilístic és un valor de probabilitat donat un exemple per cadascuna de les categories. La decisió final correspon a la categoria amb més probabilitat. Per exemple, amb la descripció anterior esperem que la sortida sigui de l'estil,<br>
# $$p( y = 'ERC' | x = (0,1,0,1,1,1)) = 0.6$$
# $$p( y = 'ICV' | x = (0,1,0,1,1,1)) = 0.4$$
# 
# <br>
# Els classificadors probabilistics Bayesians es basen en el teorema de Bayes per realitzar els càlculs per trobar la probabilitat condicionada. Es basen en el teorema de Bayes que diu:<br>
# 
# $$ p(y|x) = \frac{p(x|y)p(y)}{p(x)}$$
# <br>
# 
# 
# En molts casos $p(y)$ i $p(x)$ són desconeguts i es consideren equiprobables. Per tant, la
# decisió es simplifica a:
# <br>
# $$ p(y|x) = c · p(x|y)$$
# 
# <br>
# Les deduccions fins a aquest punt són vàlides per la majoria de classificadors Bayesians. Naïve Bayes es distingeix de la resta perquè imposa una condició encara més restrictiva. Considerem $x=(x_1,x_2,x_3,...,x_N)$ un conjunt d'$N$ variables aleatòries. Naïve Bayes assumeix que totes elles són independents entre elles i per tant podem escriure:
# 
# 
# $$p(x_1,x_2,...,x_N | y) = p(x_1|y)p(x_2|y)...p(x_N|y)$$
# 
# 
# Per tant en el nostre cas es pot veure com:
# 
# $$p(y='ERC'|x=(0,1,0,1,1,1)) = p(x_1=0|y='ERC')p(x_2=1|y='ERC')...p(x_6=1|y='ERC')$$
# 
# <br>
# Podem interpretar l'anterior equació de la següent forma: *La probabilitat de que el document descrit pel vector de característiques $(0,1,0,1,1,1)$ es proporcional al producte de la probabilitat que la primera paraula del vector no aparegui en els documents de la categoria 'ERC' per la probabilitat que la segona paraula sí aparegui als documents d''ERC', etc.*
# 
# <br>
# <b>Estimant les probabilitats marginals condicionades</b>
# L'últim pas que ens queda és trobar el valor de les probabilitats condicionades. Fem servir la representació de 0 i 1's indicant que la paraula no apareix (0) o sí apareix (1) al document. Per trobar el valor de la probabilitat condicionada farem servir una aproximació freqüentista a la probabilitat. Això vol dir que calcularem la freqüència d'aparició de cada paraula per a cada categoria. Aquest càlcul es fa dividint el nombre de documents de la categoria en que apareix la paraula pel nombre total de documents d'aquella categoria. En l'exemple anterior, $p(x_2=apareix 'verd'|y='ERC')=1/2$ mentres que $p(x_2=apareix 'verd'|y='ICV')=2/2$.
# 
# $$p(x_2 = 1 | y = 'ERC')= \frac{A}{B} $$
# 
# on $A$ és el número de blocs de 'ERC' on apareix la paraul 'verd' i $B$ és el número de bloc de 'ERC'.
# 
# 
# #### PROBLEMES A TENIR EN COMPTE:
# 
# <b> El problema de la probabilitat 0.</b>
# Si us fixeu a l'anterior exemple la probabilitat $p(x_2=no apareix 'verd' | y='ICV')$ és 0 !! Això vol dir, que si no apareix la paraula 'verd' al blog no pot ser d''ICV' !! No sembla raonable que es penalitzi totalment l'aparició d'aquesta categoria pel fet que una única paraula aparegui o no al text. Per tant el que s'acostuma a fer és donar una baixa probabilitat en comptes de zero. Una de les possibles solucions es fer servir la correcció de Laplace. Seguint l'exemple anterior la correcció de Laplace és
# 
# <br>
# $$p(x_2=1 | y = 'ERC' ) = \frac{A+1}{B+M}$$ 
# on M és el nombre de categories
# 
# <b> El problema del *'underflow'*</b>
# Es recomanable calcular sumes de logaritmes que productes.
# 
# <b> El vocabulari</b>
# L'elecció de les paraules que formen el vector de característiques és un pas crític. En funció de quan bona és aquesta descripció millor funcionarà el sistema. Tot i que us deixem a vosaltres la política de creació del vector de característiques us donem una d'exemple. Per saber quines paraules fer servir podeu seleccionar de totes les paraules de tots els blogs aquelles que apareixen entre en un 10 i un 50 percent dels blogs (sense tenir en compte la categoria). Podeu experimentar a variar aquests valors.

# #### Material per fer la pràctica
# 
# + Baixeu els fitxers python ``feedparser.py`` i ``generatefeedvector.py``. 
# El fitxer ``feedparser.py`` ens permet recuperar la informació proporcionada per sistemes de feed com RSS o Atom.   El fitxer ``generatefeedvector.py`` proporciona utilitats bàsiques per dividir el resultat de l'anterior en paraules eliminant tags d'HTML, traduint caràcters unicode i HTML corresponents al sistema de puntuació català. 
# 
# + A més trobareu el fitxer ``blogs.txt`` que conté una llista de blogs amb el següent format:  ``ID | Partit | Nom | URL``

# In[1]:

import math
import csv
import generatefeedvector as generate
import pandas as pd
import numpy as np
import operator
import copy

#Retorna un diccionari amb la seguent estricutra: {id_blog:(nom_politic,{paraula1:numAparicions;paraule2:numAparicision,...})}

def download_text(df):
    dicc={}
    invalid_blogs=[]
    #recorremos la lista con toda la informacion de todos los blogs
    maxim=df.index
    for i in maxim:
        try:
            dicc[i]=generate.getwordcounts(df.ix[i]['url'])
            #print "blog :", df.ix[i]['nom']
        except:
            invalid_blogs.append(df.index[i])
            #print "Link incorrecte, blog", df.ix[i]['nom']
    print "\nLectura dels blogs finalizada.\n",
    return dicc,invalid_blogs


# In[ ]:

# CREACIO DEL DATAFRAME 
unames = ['id_blog', 'partit_politic', 'nom', 'url']
df = pd.read_table('blogs.dat',sep='::', header=None, names=unames)

# BAIXAR UN CONJUNT DE BLOGS
data_blogs,invalid_blogs=download_text(df)
print invalid_blogs

# ELIMINAR AQUELLS BLOGS QUE NO S'HA POGUT ACCEDIR
df=df.drop(invalid_blogs)


# ## Exercici 1 
# 
# Escriure una funció ``count_blogs(dataframe)`` que retorni el nombre total de blocs als que hem pogut accedir. 

# In[ ]:

#Retorna el número total de blogs. 
def count_blogs(df):
    return (len(df))
count_blogs(df)


# ## Exercici 2:
# 
# Escriure una funció ``count_party_blogs(dataframe)`` que compte quants blogs hi ha per cadascun dels partits.

# In[ ]:

#Retorna una Series que conté el número de blogs per partit polític
def count_blogs_party(df):
    grouped= df.groupby('partit_politic')
    return grouped.count().id_blog
print count_blogs_party(df)


# ## Exercici 3:
# 
# Escriure una funció ``count_words(dataframe,data_blogs)`` que retorni un diccionari amb totes les paraules que apareixen als blogs, indicant quants cops i el nombre de blogs on ha sortit.
# 
# 
# *Possible format sortida:* ``{paraula :  {n_cops: valor; n_blogs: valor}}``

# In[ ]:

dicc=download_text(df)


# In[ ]:

# Aquesta funció ha de contruir un diccionari que contingui totes les paraules que s'han trobat indicant 
# el total de cops que ha aparescut i el nombre de blogs on apareix

def count_words(df,data_blogs):
    paraules = {}
    for blog in dicc[0]:
        for paraula in dicc[0][blog][1]:
            if paraula not in paraules.keys():
                paraules.update({paraula:{'n_cops':dicc[0][blog][1][paraula] ,'n_blogs': 1}})
            else:
                paraules[paraula]['n_cops']+=dicc[0][blog][1][paraula]
                paraules[paraula]['n_blogs']+=1
    return paraules


# In[ ]:

count_words(df,data_blogs)


# ## Exercici 4
# 
# Escriure una funció ``count_words_party(dataframe,dicc_text)`` que retorna un diccionari que conte el nombre de cops que ha aparescut cada paraula i el número de blogs on  ha aparescut. Aquesta informació ha de ser dividida en els diferents grups polítics.
# 
# *Possible format sortida:* ``{Partit_politic: {word:{n_cops: valor; n_blogs: valor}}} ``

# In[ ]:

numBlogsPartits=count_blogs_party(df)
numBlogsPartits['ERC']


# In[ ]:

#Compta la freqüència d paraules per un partit determinat.
def partit_politic(df,blog):
    partit =df.ix[blog].partit_politic
    return partit

def count_words_party(df):
    paraules = {}
    #Per cada blog:
    for blog in dicc[0]:
        #Per cada paraula que tenim en la nostre llista de blogs:
        for paraula in dicc[0][blog][1]:
            #Mirem de quin partit es tracta en aquest bl
            partit=partit_politic(df,blog)
            if partit not in paraules:
                print partit
                paraules.update({partit:{paraula:{'n_cops':dicc[0][blog][1][paraula] ,'n_blogs': 1}}})
            else:
                if paraula not in paraules[partit].keys():
                    paraules[partit].update({paraula:{'n_cops':dicc[0][blog][1][paraula] ,'n_blogs': 1}})
                else:
                    paraules[partit][paraula]['n_cops']+=dicc[0][blog][1][paraula]
                    paraules[partit][paraula]['n_blogs']+=1
    return paraules



# In[ ]:

words_partits = count_words_party(df)


# ## Exercici 5
# 
# Calcular amb la funció ``topNword(df,words_partits,N)`` quines son les $N$ paraules més representatives de cadascun dels partits. Retorneu un diccionari amb els següent format:
# 
# ``{PSC: llista_top_words_PSC; ERC: llista_top_words_ERC;...}``
# 
# Direm que una paraula és **significativa** si apareix com a màxim en un nombre $a$ de blogs i com a mínim en un nombre $b$. Per exemple, $a=50\%$ i $b=10\%$.

# In[ ]:

# Calcula les N parules més representatives de cada partit polític. La sortida ha de 
# ser un diccionari on tenim tantes entrades com partits politics
# el valors de les entrardes ha de ser una llista amb les paraules seleccionades.
def bubbleSort(keys,values,N):
    sorted = False  # We haven't started sorting yet

    while not sorted:
        sorted = True  # Assume the list is now sorted
        for i in range(0, len(values)):
            if values[i] > values[i + 1]:
                sorted = False  # We found two elements in the wrong order
                values[i],values[i+1] = values[i + 1],values[i]
                keys[i],keys[i+1] = keys[i + 1],keys[i]
                
                
def getSignificatives(a,b,paraules,N,numBlogsPartit):
    valors=[]
    paraul=[]
    for paraula in paraules:
        valor=paraules[paraula]['n_blogs']/numBlogsPartit
        if valor<=a and valor>b:
            if(len(valors)<N):
                valors.append(valor)
                paraul.append(paraula)
            elif(valor>valors[N-1]):
                 valors[N-1],paraul[N-1]=valor,paraula
            bubbleSort(paraul,valors,N)
    return paraul
                
def topNwords(df,words_partits,N):
    numBlogsPartits=count_blogs_party(df)
    words=copy.deepcopy(words_partits)
    topNwords={}
    for partit in words.keys():
        topNwords.update({partit:getSignificatives(0.5,0.1,words[partit],N,numBlogsPartits[partit])})
    return topNwords
print topNwords(df,words_partits,20)

'''
# ## Exercici 6
# 
# Creeu el vector de característiques necessari per a fer l’entrenament del Naïve Bayes (funció ``create_features()``).

# In[ ]:

# Crea el vector de característiques necessari per l'entrenament del classificador Naive Bayes
# selected_words: ha de ser el diccionari que obteniu amb la funció topNWords amb les paraules seleccionades a partir de la funció TopNwords
# data_blogs : conté el diccionari amb la informació de cada blog
# Rertorna un diccionari que conté un np.array per a cadascun dels blog amb el vector de característiques corresponent(mireu l'exemple de l'enunciat)

def create_features(data_blogs,top_words):
    return 0

N = 20 # Aquest parametre el podem canviar i fer proves per avaluar quin és el millor valorwords_partits=count_words(df,data_blogs)
top_words=topNwords(df,words_partits,N)
dict_feat_vector = create_features(data_blogs,top_words)



# In[ ]:

dict_feat_vector,vect = create_features(data_blogs,top_words)


# ## Exercici 7
# 
# Implementeu la funció d'aprenentatge del classificador Naïve Bayes (funció ``naive_bayes_learn()``).  La funció ha de mostrar el resultat obtingut per pantalla.
# 
# 
# <b> * L'error d'entrenament</b>
# L'error d'entrenament es troba calculant el percentatge d'errors que s'obtenen quan es fa el testeig amb les mateixes dades utilizades per fer entrenament (aprenentatge). Aquest error es un valor molt optimista de com funcionarà el clasificador i mai s'ha de prendre com a mesura per comparar clasificadors.
# <br>

# In[ ]:

#Mètode que implementa el clasificador Naive_Bayes.Ha de mostrar el resultat obtingut per pantalla

def naive_bayes(df,feature_vector):
    return 0

## EXEMPLE SORTIDA:
#PSC encerts: 24 / blogs: 34 / 70.59 %
#ICV encerts: 21 / blogs: 26 / 80.77 %
#CIU encerts: 29 / blogs: 32 / 90.62 %
#ERC encerts: 26 / blogs: 29 / 89.66 %
#
#Error naive bayes: 17.36 %


# ## Exercici 8
# 
# Indiqueu l'error de generalització fent servir *Leave-one-out* (funció ``leave1out()`` )
# 
# <b> * Aproximació a l'error de generalització fent servir Leave-one-out.</b>
# Una bona forma de veure com funcionaria el nostre classificador davant de dades sobre les quals no s'ha entrenat és fer servir l'estratègia leave-one-out. Aquesta estratègia entrena el classificador amb totes les dades d'entrenament menys amb una i fa el testeig sobre la dada que hem exclòs de l'entrenament. Aquest procés d'exclusió es repeteix per cadascuna de les dades d'entrenament. El percentatge d'errors fent servir aquesta estratègia permet comparar classificadors.

# In[ ]:

#Mètode per avaluar el classificador mitjançant la tècnica leave-one-out.  Ha de mostrar el resultat obtingut per pantalla

def leave1out(df,feature_vector):
     return 0
    
## EXEMPLE SORTIDA:
#PSC encerts: 24 / blogs: 34 / 70.59 %
#ICV encerts: 21 / blogs: 26 / 80.77 %
#CIU encerts: 29 / blogs: 32 / 90.62 %
#ERC encerts: 26 / blogs: 29 / 89.66 %
#
#Error naive bayes: 17.36 %
'''
