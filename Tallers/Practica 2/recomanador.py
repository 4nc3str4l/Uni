import pandas as pd
import numpy as np
import time

unames = ['user_id', 'gender', 'age', 'occupation', 'zip']
users = pd.read_table('users.dat', sep='::', header=None, names=unames, engine='python')
rnames = ['user_id', 'movie_id', 'rating', 'timestamp']
ratings = pd.read_table('ratings.dat', sep='::', header=None, names=rnames, engine='python')
mnames = ['movie_id', 'title', 'genres']
movies = pd.read_table('movies.dat', sep='::', header=None, names=mnames, engine='python')

data = pd.merge(pd.merge(ratings, users), movies)
ratings = data.pivot_table(values= 'rating', rows='title', columns='user_id')

def getFilmsWatchedBy(client_id):
    return pd.DataFrame({'rating':ratings.ix[:,client_id]})
    

def distEuclid(x, y):
    return np.sqrt(np.sum((x- y)**2))


def coefPearson(x, y):
    xmean=x.mean()
    ymean=y.mean()
    arriba= np.sum((x-xmean)*(y-ymean))
    abajo=np.sqrt(np.sum((x-xmean)**2))*np.sqrt(np.sum((y-ymean)**2))
    return round(arriba/abajo,5)


def SimEuclid(User1,User2,minFilms):
    if not User1.empty and User1.count >= minFilms:
        return 1/(1+distEuclid(User1,User2))
    else:
        return 0

def SimPearson(User1,User2,minFilms):
    C=pd.merge(User1[User1.rating.notnull()],User2[User2.rating.notnull()],left_index=True,right_index=True)
    if(C.count >= minFilms):
        return coefPearson(C['rating_x'],C['rating_y'])
    else:
        return 0
        
def getNBest(DataFrame,user,n,minFilms,similarity=SimPearson):
    usuaris_ids = DataFrame['user_id'].unique() #np.arrays amb totes les ids.
    
    
    userfilms = getFilmsWatchedBy(user)
    semb=pd.DataFrame(data={'User':usuaris_ids,'semb':0})
    ratings = np.zeros(len(usuaris_ids))
    for i in range(0,len(usuaris_ids)):
        ratings[i] = similarity(userfilms,getFilmsWatchedBy(semb.User[i]),minFilms)
    
    semb['semb'] = ratings
        

    bestUsers=[]
    trobat=False
    for i in range(n):
        index = semb['semb'].idxmax()
        if(not trobat and semb.User[index]==user):
            trobat=True
            i-=1
        else:
            bestUsers.append([semb.User[index],semb.semb[index]])
        semb.semb[index] = 0;
    return bestUsers

#Exercici 5

def getRecommendationsUser(DataFrame,person,m,n,similarity=SimPearson):
    #get the similarity between the target user and everyone.  
    mostSimUsers=getNBest(DataFrame,person,m,similarity)
    #
    ratings = data.pivot_table(values= 'rating', rows='user_id', columns='title')
    print ratings[:10]
    return 0

t1 = time.clock()
targetId = 2
comparewith = 50
minimumFilms = 10
getRecommendationsUser(data,targetId, comparewith, minimumFilms, SimEuclid)
t2 = time.clock()

print "La recomendacion ha tardado",(round(t2-t1,4),"segundos")





